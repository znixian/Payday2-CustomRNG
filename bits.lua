local Bits = {}
local BitsMeta = {}
BitsMeta.__index = Bits
BitsMeta.__tostring = function(a) return a:ToString() end
BitsMeta.__concat = function(a, b) return tostring(a) .. tostring(b) end

NiceRNG._NewBits = function()
	local b = {}
	setmetatable(b, BitsMeta)
	b:_constructor()
	return b
end

function Bits:_constructor()
	self._data = {}
	self._bits = 64

	for i=1,self._bits do
		self._data[i] = false
	end
end
function Bits:Print()
	print(self:ToString())
end
function Bits:ToString()
	local s = ""
	for i=self._bits, 1, -1 do
		if self._data[i] then
			s = s .. "1"
		else
			s = s .. "0"
		end
	end
	return s
end
function Bits:SetValue(val)
	for i=self._bits, 1, -1 do
		local bitSize = 2 ^ (i - 1)
		local bit = bitSize <= val
		self._data[i]=bit
		if bit then
		val = val - bitSize
		end
	end
end
function Bits:GetValue(bits)
	local n = 0
	for i=1, bits do
		if self._data[i] then
		n =n + 2 ^ (i - 1)
		end
	end
	return n
end
function Bits:GetData()
	return self._data
end
function Bits:SetData(data)
	self._data = data
end
function Bits:GetSize()
	return self._bits
end
function Bits:ShiftLeft(rotation)
	local new = NiceRNG._NewBits()

	for i = rotation + 1, self._bits do
		new._data[i] = self._data[i - rotation]
	end

	return new
end
function Bits:ShiftRight(rotation)
	local new = NiceRNG._NewBits()
	
	for i=1, self._bits - rotation do
		new._data[i] = self._data[i + rotation]
	end
	
	return new
end
function Bits:XOrLocal(other)
	self:XOr(self, other)
end
function Bits:XOr(a, b) 
	for i=1, self._bits do
		self._data[i] = a._data[i] ~= b._data[i]
	end
end
function Bits:OrLocal(other)
	self:Or(self, other)
end
function Bits:Or(a, b) 
	for i=1, self._bits do
		self._data[i] = a._data[i] or b._data[i]
	end
end
function Bits:Clone()
	local new = NiceRNG._NewBits()

	for i= 1, self._bits do
		new._data[i] = self._data[i]
	end

	return new
end
