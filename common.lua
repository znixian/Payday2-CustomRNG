if NiceRNG == nil then
	NiceRNG = {}
	
	NiceRNG._path = ModPath
	NiceRNG._seedfile_path = SavePath .. "NiceRNG.seed"
	NiceRNG._loaded = false
	
	dofile(NiceRNG._path .. "static.lua")
	
	function NiceRNG:GetSeed()
		self:_Load()
		
		if self._seed == nil then
			self:RandomizeSeed()
			return self._seed_rand
		end
		return self._seed
	end
	
	function NiceRNG:GetModeId()
		self:_Load()
		return self._mode or 1
	end
	
	function NiceRNG:GetMode()
		return NiceRNG.Modes[NiceRNG:GetModeId()]
	end
	
	function NiceRNG:SaveSeed(seed)
		self:_Load() -- so we don't mess up anything else when saving
		
		if seed == "" then seed = nil end
		
		self._seed = seed
		self:ResetRNG()
		self:_Save()
	end
	
	function NiceRNG:SaveMode(mode)
		self:_Load() -- so we don't mess up anything else when saving
		
		mode = mode or 1
		
		self._mode = mode
		self:ResetRNG()
		self:_Save()
	end
	
	function NiceRNG:_Save()
		local file = io.open( NiceRNG._seedfile_path, "w+" )
		if file then
			local data = {
				placeholder = 1, -- make sure there is always an entry
				seed = NiceRNG._seed,
				mode = NiceRNG._mode
			}
			
			file:write( json.encode( data ) )
			file:close()
		end
	end
	
	function NiceRNG:_Load()
		if self._loaded then return end
		
		local file = io.open( NiceRNG._seedfile_path, "r" )
		if file then
			local data = json.decode( file:read("*all") )
			self:ResetRNG()
			self._seed = data.seed
			self._mode = data.mode or 1
			self._loaded = true
			file:close()
		end
	end
	
	function NiceRNG:RandomizeSeed()
		if self._seed_rand ~= nil then return end
		
		local chars = ""
		
		local a = string.byte('a')
		local z = string.byte('z')
		
		for i=1,15 do
			chars = chars .. string.char(math.random(a, z))
		end
		
		self._seed_rand = chars -- so we don't save it
	end
	
	function NiceRNG:ResetRNG()
		self._prng = nil
	end
	
	function NiceRNG:GetNewPRNG()
		self._prng = self.Static:GetNewPRNG(self:GetSeed(), self:GetMode())
		return self._prng
	end
	
	function NiceRNG:GetPRNG()
		if self._prng == nil then return self:GetNewPRNG() end
		return self._prng
	end
end
