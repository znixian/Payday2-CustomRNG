local mdl = core:_name_to_module("CoreElementLogicChance")

function mdl.ElementLogicChance:on_executed(instigator)
	local prng = NiceRNG:GetPRNG()
	prng:start(self._id)
	local roll = prng:random(100)
	
-- 	local name = (roll > self._chance) and "fail" or "success"
-- 	log("[NiceRNG] ElementLogicChance: " .. self._id .. "." .. self._editor_name .. ": " .. self._chance .. "% for " .. name)
	
	if roll > self._chance then
		self:_trigger_outcome("fail")
		return
	end
	self:_trigger_outcome("success")
	mdl.ElementLogicChance.super.on_executed(self, instigator)
end
