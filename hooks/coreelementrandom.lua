local mdl = core:_name_to_module("CoreElementRandom")

local old_on_exec = mdl.ElementRandom.on_executed
function mdl.ElementRandom:on_executed(...)
	NiceRNG:GetPRNG():start(self._id)
	return old_on_exec(self, ...)
end

function mdl.ElementRandom:_calc_amount()
	local amount = self._values.amount or 1
	if self._values.amount_random and self._values.amount_random > 0 then
		amount = amount + NiceRNG:GetPRNG():random(self._values.amount_random + 1) - 1
	end
-- 	log("[NiceRNG] ElementRandom: " .. self._id .. "." .. self._editor_name .. " = " .. amount)
	return amount
end

function mdl.ElementRandom:_get_random_elements()
	local rand = NiceRNG:GetPRNG():random(#self._unused_randoms)
	
-- 	if self._id == 100726 then rand = 2 end
-- 	rand = 1
-- 	if #self._unused_randoms == 2 and self._id < 105112 then
-- 		rand = 2
-- 		log("[NiceRNG] ElementRandom: " .. self._id .. "." .. self._editor_name .. " to 2")
-- 	end
	
-- 	log("[NiceRNG] ElementRandom: " .. self._id .. "." .. self._editor_name .. " using " .. rand .. " of " .. #self._unused_randoms)
	return table.remove(self._unused_randoms, rand)
end
