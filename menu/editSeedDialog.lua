--[[
    Edit Seed Dialog
   
   Most of this code is copied with modification
   from James Wilkinson's GoonMod mod, licensed
   under the MIT license. It came from the following files:
   - dialogs/RedeemCodeDialog.lua
   - dialogs/ScalableTextBoxGUI.lua
]]--

require("lib/managers/dialogs/GenericDialog")
require("lib/managers/menu/TextBoxGui")
local GenericDialog = core:_name_to_module("SystemMenuManager").GenericDialog

local text_empty = "NiceRNG_setSeed_text_empty"
local tweak_data = _G.tweak_data
local code_color_normal = Color.white
local code_color_no_code = Color.white:with_alpha(0.3)
local mode_colour = Color(255, 0, 166, 81) / 255
local max_code_length = 15

local function make_fine_text(text)
	local x, y, w, h = text:text_rect()
	text:set_size(w, h)
	text:set_position(math.round(text:x()), math.round(text:y()))
	return text:x(), text:y(), w, h
end

-- Scalable Text Box (dialogs/ScalableTextBoxGUI.lua)
local ScalableTextBoxGui = ScalableTextBoxGui or class( TextBoxGui )

function ScalableTextBoxGui:set_size(x, y)

	ScalableTextBoxGui.super.set_size(self, x, y)

	local padding = 10
	local info_area = self._panel:child("info_area")
	local buttons_panel = info_area:child("buttons_panel")
	local scroll_panel = info_area:child("scroll_panel")
	scroll_panel:set_w( info_area:w() - scroll_panel:x() * 2 )
	scroll_panel:set_h( info_area:h() - buttons_panel:h() - padding * 3 )

	make_fine_text( scroll_panel:child("text") )

	buttons_panel:set_right( info_area:right() - padding )
	buttons_panel:set_bottom( info_area:bottom() - padding )

end

-- Seed Dialog (dialogs/RedeemCodeDialog.lua)
local SeedDialog = NiceRNG.SeedDialog or class(GenericDialog)
NiceRNG.SeedDialog = SeedDialog

local function getTextboxLocation(self)
	return self._panel_script:w() * 0.5, self._panel_script:h() * 0.5
end

function SeedDialog:init(manager, data)
	GenericDialog.init(self, manager, data)
	if not self._data.focus_button then
		if #self._button_text_list > 0 then
			self._data.focus_button = #self._button_text_list
		else
			self._data.focus_button = 1
		end
	end
	self._ws = self._data.ws or manager:_get_ws()
	self._panel_script = ScalableTextBoxGui:new(self._ws, self._data.title or "", (self._data.text or ""), self._data, {
		no_close_legend = true,
		use_indicator = data.indicator or data.no_buttons,
		is_title_outside = is_title_outside,
	})
	self._panel_script:add_background()
	self._panel_script:set_layer(tweak_data.gui.DIALOG_LAYER)
	self._panel_script:set_fade(0)
	self._panel_script:set_size( 540, 360 )
	self._panel_script:set_centered()
	self._controller = self._data.controller or manager:_get_controller()
	self._confirm_func = callback(self, self, "button_pressed_callback")
	self._cancel_func = callback(self, self, "dialog_cancel_callback")
	self._resolution_changed_callback = callback(self, self, "resolution_changed_callback")
	managers.viewport:add_resolution_changed_func(self._resolution_changed_callback)
	if data.counter then
		self._counter = data.counter
		self._counter_time = self._counter[1]
	end
	
	self._panel = self._panel_script._panel
	
	local layer = tweak_data.gui.MOUSE_LAYER - 50
	local w, h = self._panel_script:w() * 0.85, self._panel_script:h() * 0.3
	local x, y = getTextboxLocation(self)
	
	self._code_rect = self._panel:rect({
		w = w,
		h = h,
	})
	self._code_rect:set_color( Color.black:with_alpha(0.5) )
	self._code_rect:set_center( x, y )
	
	-- main piece of text
	self._code_text = self._panel:text({
		name = "code_text",
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.menu.pd2_large_font_size,
		blend_mode = "add",
		layer = layer
	})
	self._code_text:set_color( code_color_no_code )
	make_fine_text( self._code_text )
	self._code_text:set_center( x, y )
	
	-- indicator text
	local indicator_size = "small"
	self._indicator_text = self._panel:text({
		name = "indicator_text",
		font = tweak_data.menu["pd2_" .. indicator_size .. "_font"],
		font_size = tweak_data.menu["pd2_" .. indicator_size .. "_font_size"],
		blend_mode = "add",
		layer = layer
	})
	self._indicator_text:set_color( mode_colour )
	
	-- set up the keyboard handling
	if not self._connected_keyboard then
		self._ws:connect_keyboard(Input:keyboard())
		self._panel:enter_text(callback(self, self, "enter_text"))
		self._panel:key_press(callback(self, self, "key_press"))
	
		self._connected_keyboard = true
	end
	
	-- set the seed, and update the shown text and mode indicator
	self._entered_code = self._data.seed or "" -- load the supplied seed
	self._selected_mode = self._data.mode or 1
	self:UpdateCodeText()
	self:UpdateModeIndicator()
end

function SeedDialog:UpdateCodeText()
	if not alive( self._code_text ) then
		return
	end
	
	self._code_text:set_text( tostring(self._entered_code):upper() )
	if self._entered_code:is_nil_or_empty() then
		self._code_text:set_color( code_color_no_code )
		self._code_text:set_text( managers.localization:text(text_empty) )
	else
		self._code_text:set_color( code_color_normal )
	end
	
	local x, y = getTextboxLocation(self)
	make_fine_text( self._code_text )
	self._code_text:set_center( x, y )
	
	self._data.seed = self._entered_code
end

function SeedDialog:UpdateModeIndicator()
	if not alive( self._indicator_text ) then
		return
	end
	
	self._indicator_text:set_text(NiceRNG.Modes[self._selected_mode].name)
	
	local x = self._code_rect:x()
	local y = self._code_rect:y()
	
	self._indicator_text:set_position(math.round(x + 10), math.round(y + 10))
	
	self._data.mode = self._selected_mode
end

function SeedDialog:enter_text( o, s )
	local n = utf8.len(self._entered_code)
	if n < max_code_length then
		self._entered_code = self._entered_code .. tostring(s)
		self._entered_code = self._entered_code:lower()
	end
	
	self:UpdateCodeText()
end

function SeedDialog:key_press( o, k )
	if k == Idstring("backspace") then
		local n = utf8.len(self._entered_code)
		self._entered_code = utf8.sub(self._entered_code, 0, math.max(n - 1, 0))
		self:UpdateCodeText()
	elseif k == Idstring("tab") then
		local mode = self._selected_mode
		
		mode = mode + 1
		if mode > #NiceRNG.Modes then
			mode = 1
		end
		
		self._selected_mode = mode
		self:UpdateModeIndicator()
	end
end

function SeedDialog:close()
	if self._connected_keyboard then
		self._ws:disconnect_keyboard()
		self._panel:enter_text(nil)
		self._panel:key_press(nil)
		self._connected_keyboard = false
	end
	
	SeedDialog.super.close(self)
end
