local LANG = "en"
local path = ModPath

local uniqId = "NiceRNG"
local targetMenu = "options"

dofile(path .. "common.lua")
dofile(path .. "menu/editSeedDialog.lua")

--[[
	Load our localization keys for our menu, and menu items.
]]
Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_" .. uniqId, function( loc )
	loc:load_localization_file(path .. "lang/" .. LANG .. ".translation")
end)

local function show(data)
	local m = managers.system_menu
	
	if NiceRNG.SeedDialog == nil then
	end
	
	local success = m:_show_class(data, NiceRNG.SeedDialog, NiceRNG.SeedDialog, data.force) -- m.GENERIC_DIALOG_CLASS, m.DIALOG_CLASS
	managers.system_menu:_show_result(success, data)
end

local function show_set_seed_dialog(params)
	local dialog_data = {}
	dialog_data.title = managers.localization:text(uniqId .. "_setSeed_title")
	dialog_data.text = managers.localization:text(uniqId .. "_setSeed_desc")
	
	dialog_data.seed = NiceRNG:GetSeed()
	dialog_data.mode = NiceRNG:GetModeId()
	
	local yes_button = {}
	yes_button.text = managers.localization:text("dialog_yes")
	yes_button.callback_func = function(...) params.yes_func(dialog_data, ...) end
	dialog_data.button_list = {yes_button}
	
	local cancel_button = {}
	cancel_button.text = managers.localization:text("dialog_cancel")
	cancel_button.callback_func = function(...) end
	
	dialog_data.button_list = {yes_button, cancel_button}
	dialog_data.focus_button = 1
	show(dialog_data)
end
 
-- Add our menu
Hooks:Add( "MenuManagerInitialize", "MenuManagerInitialize_" .. uniqId , function( menu_manager )
	local LocalCallbackHandler = {}
	function LocalCallbackHandler:ShowMenu(self, item)
		show_set_seed_dialog({
			yes_func = function(data, id, bttn_data)
				NiceRNG:SaveSeed(data.seed)
				NiceRNG:SaveMode(data.mode)
			end
		})
	end
	
	-- Insert in menu
	Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenus_" .. uniqId, function(menu_manager, nodes)
		-- The menu we want to add to
		local menu = nodes[targetMenu]
		
		-- Make a button
		local button = menu:create_item({
			type = "CoreMenuItem.Item",
		}, {
			name = uniqId,
			text_id = uniqId .. "_menu_title",
			help_id = uniqId .. "_menu_desc",
			callback = "ShowMenu",
			back_callback = nil,
			disabled_color = Color(0.25, 1, 1, 1),
			next_node = nil,
			localize = true
		})
		
		button:set_callback_handler(LocalCallbackHandler)
		
		local position = nil
		
		-- Find the position of the menu
		for k, v in pairs( menu._items ) do
			if "lua_mod_options_menu" == v["_parameters"]["name"] then
				position = k + 1
				break
			end
		end
		
		-- Add the button to the menu
		table.insert( menu._items, position, button )
	end)
end)
