local Mode = {}

Mode.name = "Event-Seperate"

function Mode:init(seeder)
	self._seeder = seeder
end

function Mode:start(id)
	self._prng = NiceRNG._NewPRNG(self._seeder(id))
-- 	if log == nil then
-- 		print(id, self._prng._bits)
-- 	else
-- 		log(id .. ":" .. self._prng._bits)
-- 	end
	
-- 	local bits = NiceRNG._NewBits()
-- 	bits:SetValue(id)
-- 	
-- 	self._prng:GetBits():XOrLocal(bits:ShiftLeft(15))
-- 	
-- 	for i=1, 5 do
-- 		self._prng:Advance()
-- 	end
end

function Mode:random(max)
	return self._prng:random(max)
end

table.insert(NiceRNG.Modes, Mode)
