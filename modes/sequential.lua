local Mode = {}

Mode.name = "Sequential"

function Mode:init(seeder)
	self._prng = NiceRNG._NewPRNG(seeder(""))
end

function Mode:start(id)
end

function Mode:random(max)
	return self._prng:random(max)
end

table.insert(NiceRNG.Modes, Mode)
