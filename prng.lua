local PRNG = {}
PRNG.__index = PRNG

-- see http://javamex.com/tutorials/random_numbers/xorshift.shtml

function PRNG:_constructor(seed)
	self._bits = seed:Clone()
	
	for i=1,10 do self:Advance() end
end

function PRNG:Advance()
	self._bits:XOrLocal(self._bits:ShiftLeft(21))
	self._bits:XOrLocal(self._bits:ShiftRight(35))
	self._bits:XOrLocal(self._bits:ShiftLeft(4))
end

function PRNG:GetBits()
	return self._bits
end

function PRNG:SetBits(bits)
	self._bits = bits
end

function PRNG:random(max)
	self:Advance()
	local num = self._bits:GetValue(10)
	num = num % max + 1
-- 	log("[PRNG] Generating: " .. max .. ", got " .. num)
	return num
end

NiceRNG._NewPRNG = function(seed)
	local b = {}
	setmetatable(b, PRNG)
	b:_constructor(seed)
	return b
end
