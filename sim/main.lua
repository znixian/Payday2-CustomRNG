local SEED = "hello-worlds"
local MODE = 2 -- event seperate
local HEIST = "bank_cash"

NiceRNG = {}
NiceRNG._path = "../"

dofile(NiceRNG._path .. "static.lua")

print(SEED, MODE, HEIST)

Sim = {
	rng = NiceRNG.Static:GetNewPRNG(SEED, NiceRNG.Modes[MODE])
}

-- CoreElementRandom simulator
function Sim:CER(id, name, amount, amount_random, outcomes)
	self.rng:start(id)
	
	if aamount_random and amount_random > 0 then
		amount = amount + self.rng:random(amount_random + 1) - 1
	end
	
	local results = {}
	
	for i = 1, amount do
		if outcomes ~= nil then
			local id = self.rng:random(#outcomes)
			
			table.insert(results, outcomes[id])
			table.remove(outcomes, id)
		else
			self.rng:random(1)
		end
	end
	
	if outcomes ~= nil then
		print(name, table.concat(outcomes, ","))
	else
		print(name, amount)
	end
end

-- CoreElementLogicChance simulator
function Sim:CELC(id, name, chance)
	self.rng:start(id)
	local roll = self.rng:random(100)
	
	local result = (roll <= chance) and "true" or "false"
	print(name, result)
end

dofile("heists/" .. HEIST .. ".lua")
