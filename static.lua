-- Modes
NiceRNG.Modes = {}

for _, name in pairs({
	"sequential",
	"eventseperate"
}) do
	dofile(NiceRNG._path .. "modes/" .. name .. ".lua")
end

-- Static
NiceRNG.Static = {}
local Static = NiceRNG.Static

function Static:GetNewPRNG(seed, mode)
	if NiceRNG._NewBits == nil then
		dofile(NiceRNG._path .. "bits.lua")
		dofile(NiceRNG._path .. "prng.lua")
		dofile(NiceRNG._path .. "sha2.lua")
	end
	
	local prng = {}
	setmetatable(prng, {__index = mode})
	prng:init(function(buffer)
		return NiceRNG._SHA256(seed .. buffer)
	end)
	
	return prng
end
